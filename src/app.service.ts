import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { Response } from 'express';
import { BUILD_FOLDER, API_FILE } from './config/constants';
import { IsFileExists, sendFileByResponse } from './utils/handlers/file.handler';

@Injectable()
export class AppService {
	getHello(): string {
		return 'Hello World!';
	}

	async getApiDocument(res: Response): Promise<void> {
		try {
			const data = await IsFileExists(`${BUILD_FOLDER}/${API_FILE}`);
			if (!data) {
				throw new NotFoundException('File not found');
			}
			return sendFileByResponse(res, API_FILE, BUILD_FOLDER);
		} catch (_error) {
			throw new HttpException(_error, HttpStatus.FORBIDDEN);
		}
	}
}
