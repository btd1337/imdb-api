import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { AppService } from './app.service';

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) {}

	@Get()
	getHello(): string {
		return this.appService.getHello();
	}

	@Get('collections')
	@ApiOperation({ summary: 'Get API docs file' })
	@ApiResponse({ status: HttpStatus.OK, description: 'Response contains an json file' })
	@ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'File not found' })
	async getApiDocument(@Res() res: Response): Promise<void> {
		return this.appService.getApiDocument(res);
	}
}
