import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { validatePassword } from 'src/utils/handlers/number.handler';
import { isRequiredMessage, notFoundMessage } from 'src/utils/messages/default.message';
import { QueryFilter } from 'src/utils/query-filter.interface';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { SignInDto } from '../auth/dto/signin.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService extends TypeOrmCrudService<User> {
	constructor(@InjectRepository(User) repository: Repository<User>) {
		super(repository);
	}

	async createOne(req: CrudRequest, dto: CreateUserDto): Promise<User> {
		const user = new User(dto);
		user.salt = await bcrypt.genSalt();
		user.password = await bcrypt.hash(dto.password, user.salt);
		return this.repo.save(user);
	}

	async deleteOne(req: CrudRequest): Promise<User> {
		const id: QueryFilter = req.parsed.paramsFilter.find((value) => value.field === 'id');
		if (!id) {
			throw new BadRequestException(isRequiredMessage('Id'));
		}

		const user: User = await this.repo.findOne(id.value);

		if (!user) {
			throw new NotFoundException(notFoundMessage('User'));
		}

		user.isDeleted = true;
		return this.repo.save(user);
	}

	async validatePasswordAndGet(dto: SignInDto): Promise<User> {
		const { email, password } = dto;
		const user = await this.repo.findOne({ email });

		if (user && (await validatePassword(password, user.password, user.salt))) {
			return user;
		}
		return null;
	}
}
