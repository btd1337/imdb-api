import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length, Matches } from 'class-validator';
import {
	MIN_NAME_LENGTH,
	MAX_NAME_LENGTH,
	MAX_LAST_NAME_LENGTH,
	MIN_LAST_NAME_LENGTH,
	MAX_USER_PASSWORD_LENGTH,
	MIN_USER_PASSWORD_LENGTH,
	MIN_EMAIL_LENGTH,
	MAX_EMAIL_LENGTH,
} from 'src/config/constants';

export class CreateUserDto {
	@IsString()
	@Length(MIN_NAME_LENGTH, MAX_NAME_LENGTH)
	@IsNotEmpty()
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	name: string;

	@IsString()
	@Length(MIN_LAST_NAME_LENGTH, MAX_LAST_NAME_LENGTH)
	@IsNotEmpty()
	@ApiProperty({
		type: String,
		minLength: MIN_LAST_NAME_LENGTH,
		maxLength: MAX_LAST_NAME_LENGTH,
	})
	lastName: string;

	@IsEmail()
	@Length(MIN_EMAIL_LENGTH, MAX_EMAIL_LENGTH)
	@IsNotEmpty()
	@ApiProperty({
		type: String,
		minLength: MIN_EMAIL_LENGTH,
		maxLength: MAX_EMAIL_LENGTH,
	})
	email: string;

	@IsString()
	@Matches(/(?=^.{8,32}$)((?=.*\d)|(?=.*\W+))(?=.*[0-9])(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
		message: `Passwords must contain at least: ${MIN_USER_PASSWORD_LENGTH} and maximum ${MAX_USER_PASSWORD_LENGTH} characters: 1 uppercase letter, 1 lowercase letter and 1 number`,
	})
	@Length(MIN_USER_PASSWORD_LENGTH, MAX_USER_PASSWORD_LENGTH)
	@IsNotEmpty()
	@ApiProperty({
		type: String,
		minLength: MIN_USER_PASSWORD_LENGTH,
		maxLength: MAX_USER_PASSWORD_LENGTH,
	})
	password: string;
}
