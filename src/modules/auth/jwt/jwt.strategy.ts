import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { JWT_CONSTANTS } from 'src/config/constants';
import { AdminsService } from 'src/modules/admins/admins.service';
import { Admin } from 'src/modules/admins/entities/admin.entity';
import { User } from 'src/modules/users/entities/user.entity';
import { UsersService } from 'src/modules/users/users.service';
import { UserType } from 'src/utils/enums/user-type.enum';
import { invalidTypeMessage, notFoundMessage } from 'src/utils/messages/default.message';
import { JwtPayload } from './jwt-payload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(private adminsService: AdminsService, private usersService: UsersService) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: true,
			secretOrKey: JWT_CONSTANTS.secret,
		});
	}

	async validate(payload: JwtPayload): Promise<Admin | User> {
		const { email, userType } = payload;
		switch (userType) {
			case UserType.ADMIN: {
				const admin = await this.adminsService.findOne({ email });
				if (!admin) {
					throw new NotFoundException(notFoundMessage('Admin'));
				}
				return admin;
			}

			case UserType.USER: {
				const user = await this.usersService.findOne({ email });
				if (!user) {
					throw new NotFoundException(notFoundMessage('User'));
				}
				return user;
			}

			default: {
				throw new BadRequestException(invalidTypeMessage('User Type'));
			}
		}
	}
}
