import { JwtService } from '@nestjs/jwt';
import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { UserType } from 'src/utils/enums/user-type.enum';
import {
	notFoundMessage,
	unauthorizedCredentialsMessage,
} from 'src/utils/messages/default.message';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { SignInDto } from './dto/signin.dto';
import { JwtPayload } from './jwt/jwt-payload.interface';
import { SignInResponseDto } from './dto/signin-response.dto';
import { Admin } from '../admins/entities/admin.entity';
import { AdminsService } from '../admins/admins.service';

@Injectable()
export class AuthService {
	constructor(
		private jwtService: JwtService,
		private adminsService: AdminsService,
		private usersService: UsersService,
	) {}

	async signInUser(dto: SignInDto): Promise<SignInResponseDto> {
		dto.email = dto.email.toLowerCase();
		const { email } = dto;

		const user: User = await this.usersService.validatePasswordAndGet(dto);

		if (!user) {
			throw new UnauthorizedException(unauthorizedCredentialsMessage);
		}
		if (user.isDeleted) {
			throw new NotFoundException(notFoundMessage('User'));
		}

		const userType = UserType.USER;
		const payload: JwtPayload = { email, userType };
		const accessToken = this.jwtService.sign(payload);
		return { accessToken, id: user.id };
	}

	async signInAdmin(dto: SignInDto): Promise<SignInResponseDto> {
		dto.email = dto.email.toLowerCase();
		const { email } = dto;

		const admin: Admin = await this.adminsService.validatePasswordAndGet(dto);

		if (!admin) {
			throw new UnauthorizedException(unauthorizedCredentialsMessage);
		}
		if (admin.isDeleted) {
			throw new NotFoundException(notFoundMessage('Admin'));
		}

		const userType = UserType.ADMIN;
		const payload: JwtPayload = { email, userType };
		const accessToken = this.jwtService.sign(payload);
		return { accessToken, id: admin.id };
	}
}
