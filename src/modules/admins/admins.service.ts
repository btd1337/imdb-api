import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { validatePassword } from 'src/utils/handlers/number.handler';
import { isRequiredMessage, notFoundMessage } from 'src/utils/messages/default.message';
import { QueryFilter } from 'src/utils/query-filter.interface';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { SignInDto } from '../auth/dto/signin.dto';
import { Admin } from './entities/admin.entity';

@Injectable()
export class AdminsService extends TypeOrmCrudService<Admin> {
	constructor(@InjectRepository(Admin) repository: Repository<Admin>) {
		super(repository);
	}

	async createOne(req: CrudRequest, dto: Admin): Promise<Admin> {
		const admin = new Admin(dto);
		admin.salt = await bcrypt.genSalt();
		admin.password = await bcrypt.hash(dto.password, admin.salt);
		return this.repo.save(admin);
	}

	async deleteOne(req: CrudRequest): Promise<Admin> {
		const id: QueryFilter = req.parsed.paramsFilter.find((value) => value.field === 'id');
		if (!id) {
			throw new BadRequestException(isRequiredMessage('Id'));
		}

		const admin: Admin = await this.repo.findOne(id.value);

		if (!admin) {
			throw new NotFoundException(notFoundMessage('Admin'));
		}

		admin.isDeleted = true;
		return this.repo.save(admin);
	}

	async validatePasswordAndGet(dto: SignInDto): Promise<Admin> {
		const { email, password } = dto;
		const admin = await this.repo.findOne({ email });
		if (admin && (await validatePassword(password, admin.password, admin.salt))) {
			return admin;
		}
		return null;
	}
}
