import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsString, Length } from 'class-validator';
import {
	MIN_NAME_LENGTH,
	MAX_NAME_LENGTH,
	MIN_LAST_NAME_LENGTH,
	MAX_LAST_NAME_LENGTH,
	MAX_EMAIL_LENGTH,
	MIN_EMAIL_LENGTH,
} from 'src/config/constants';

export class UpdateAdminDto {
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
		required: false,
	})
	@IsString()
	@Length(MIN_NAME_LENGTH, MAX_NAME_LENGTH)
	@IsOptional()
	name?: string;

	@ApiProperty({
		type: String,
		minLength: MIN_LAST_NAME_LENGTH,
		maxLength: MAX_LAST_NAME_LENGTH,
		required: false,
	})
	@IsString()
	@Length(MIN_LAST_NAME_LENGTH, MAX_LAST_NAME_LENGTH)
	@IsOptional()
	lastName?: string;

	@ApiProperty({
		type: String,
		minLength: MIN_EMAIL_LENGTH,
		maxLength: MAX_EMAIL_LENGTH,
		required: false,
	})
	@IsEmail()
	@Length(MIN_EMAIL_LENGTH, MAX_EMAIL_LENGTH)
	@IsOptional()
	email?: string;
}
