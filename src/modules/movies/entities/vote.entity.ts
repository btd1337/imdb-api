import { ApiProperty } from '@nestjs/swagger';
import { MAX_MOVIE_RATING, MIN_MOVIE_RATING, UUID } from 'src/config/constants';
import { User } from 'src/modules/users/entities/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Movie } from './movie.entity';

@Entity()
@Unique(['movie', 'user'])
export class Vote {
	@PrimaryGeneratedColumn('uuid')
	@ApiProperty(UUID)
	id: string;

	@Column({ type: 'int' })
	@ApiProperty({
		type: 'number',
		minimum: MIN_MOVIE_RATING,
		maximum: MAX_MOVIE_RATING,
	})
	rating: number;

	@ManyToOne(() => Movie, (movie) => movie.votes)
	@ApiProperty({ type: Movie })
	movie: Movie;

	@ManyToOne(() => User, (user) => user.votes)
	@ApiProperty({ type: () => User })
	user: User;
}
