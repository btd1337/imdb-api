import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';
import { MAX_NAME_LENGTH, MIN_NAME_LENGTH } from 'src/config/constants';

export class UpdateDirectorDto {
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	@IsString()
	@Length(MIN_NAME_LENGTH, MAX_NAME_LENGTH)
	@IsNotEmpty()
	name: string;
}
