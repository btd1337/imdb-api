import { Controller, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { RolesGuard } from 'src/utils/guards/roles.guard';
import { CreateDirectorDto } from '../dto/create-director.dto';
import { UpdateDirectorDto } from '../dto/update-director.dto';
import { Director } from '../entities/director.entity';
import { DirectorsService } from './directors.service';

@Crud({
	model: {
		type: Director,
	},
	dto: {
		create: CreateDirectorDto,
		update: UpdateDirectorDto,
	},
})
@Controller('directors')
@UseGuards(
	AuthGuard('jwt'),
	new RolesGuard({
		createOneRoles: ['admin'],
		updateOneRoles: ['admin'],
		deleteOneRoles: ['admin'],
	}),
)
@ApiTags('directors')
@ApiBearerAuth()
export class DirectorsController implements CrudController<Director> {
	constructor(public service: DirectorsService) {}
}
