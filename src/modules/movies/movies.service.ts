import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Movie } from './entities/movie.entity';

@Injectable()
export class MoviesService extends TypeOrmCrudService<Movie> {
	constructor(@InjectRepository(Movie) repository: Repository<Movie>) {
		super(repository);
	}
}
