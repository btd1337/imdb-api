import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Vote } from '../entities/vote.entity';

@Injectable()
export class VotesService extends TypeOrmCrudService<Vote> {
	constructor(@InjectRepository(Vote) repository: Repository<Vote>) {
		super(repository);
	}
}
