export const APP_NAME = 'IMDB_API';
export const API_VERSION = 1.0;
export const BUILD_FOLDER = 'dist';
export const API_FILE = `${APP_NAME}-api-v${API_VERSION}.json`;

// Database
export const UUID_LENGTH = 36;
export const DECIMAL_PLACES = 2;

// Auth
export const JWT_CONSTANTS = {
	secret: 'topSecret51',
};

// Validators
export const UUID = {
	type: 'string',
	minLength: UUID_LENGTH,
	maxLength: UUID_LENGTH,
	example: '95e3e065-009b-4b38-baa9-a26116548b15',
};

// User
export const MIN_NAME_LENGTH = 3;
export const MAX_NAME_LENGTH = 32;
export const MIN_LAST_NAME_LENGTH = 3;
export const MAX_LAST_NAME_LENGTH = 128;
export const MIN_USER_PASSWORD_LENGTH = 8;
export const MAX_USER_PASSWORD_LENGTH = 32;
export const MIN_EMAIL_LENGTH = 3;
export const MAX_EMAIL_LENGTH = 254;

// Movies
export const MIN_MOVIE_RATING = 0;
export const MAX_MOVIE_RATING = 4;
