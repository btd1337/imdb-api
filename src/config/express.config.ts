const expressDefaultPort = 3000;

export default {
	environment: process.env.EXPRESS_ENVIRONMENT,
	port: process.env.EXPRESS_PORT || expressDefaultPort,

	// helpers
	isProduction(): boolean {
		return this.get('express').environment === 'production';
	},
};
