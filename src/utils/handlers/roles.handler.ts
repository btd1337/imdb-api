export function matchRoles(userRoles: string[], verifiedRoles: string[]): boolean {
	for (let i = 0; i < userRoles.length; i++) {
		if (verifiedRoles.includes(userRoles[i])) {
			return true;
		}
	}
	return false;
}
