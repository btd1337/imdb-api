import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { getAction } from '@nestjsx/crud';
import { matchRoles } from '../handlers/roles.handler';

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(
		//   private readonly reflector: Reflector,
		public restfulCrudOptions: {
			readAllRoles?: string[];
			readOneRoles?: string[];
			createOneRoles?: string[];
			updateOneRoles?: string[];
			deleteOneRoles?: string[];
		},
	) {}

	canActivate(context: ExecutionContext): boolean {
		const request = context.switchToHttp().getRequest();
		const handler = context.getHandler();
		const action = getAction(handler);
		// const controller = context.getClass();
		// const feature = getFeature(controller)
		const { user } = request;

		switch (action) {
			case 'Read-All': {
				if (!this.restfulCrudOptions.readAllRoles) {
					return true;
				}
				return matchRoles(user.roles, this.restfulCrudOptions.readAllRoles);
			}

			case 'Read-One': {
				if (!this.restfulCrudOptions.readOneRoles) {
					return true;
				}
				return matchRoles(user.roles, this.restfulCrudOptions.readOneRoles);
			}

			case 'Create-One': {
				if (!this.restfulCrudOptions.createOneRoles) {
					return true;
				}
				return matchRoles(user.roles, this.restfulCrudOptions.createOneRoles);
			}

			case 'Update-One': {
				if (!this.restfulCrudOptions.updateOneRoles) {
					return true;
				}
				return matchRoles(user.roles, this.restfulCrudOptions.updateOneRoles);
			}

			case 'Delete-One': {
				if (!this.restfulCrudOptions.deleteOneRoles) {
					return true;
				}
				return matchRoles(user.roles, this.restfulCrudOptions.deleteOneRoles);
			}

			default: {
				return true;
			}
		}
	}
}
